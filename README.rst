Luigi Tasks
===========

Some *Luigi Tasks* to ease your day.


Config Example
--------------

Here is my config (just kidding):

.. code::

    [luigi_tasks]
    cachedir = /home/toopy/.cache/luigi_tasks


Pipeline Tasks
--------------

Small addition to the luigi patterns, here is the parents principle:

.. shell::

    $ luigi_tasks Ping --Ping-parents '["Pong"]'


Crontab Example
---------------

Here is how I schedule some tasks:

.. code::

    LUIGI_CONFIG_PATH=.luigi/default.cfg
    * * * * * luigi_tasks AnsiblePlaybook ... 2> .cronlogs/luigi_tasks.AnsiblePlaybook_github.log
