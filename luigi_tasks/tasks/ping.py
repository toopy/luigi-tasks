from luigi_tasks.tasks.base import ConfigParameter, TaskBase


class Ping(TaskBase):
    ping = ConfigParameter()

    def run(self):
        with self.output().open("w") as f:
            f.write("{}\n".format(self.ping))
