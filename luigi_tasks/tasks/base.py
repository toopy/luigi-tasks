import os
from datetime import datetime

import luigi
from cached_property import cached_property
from luigi_tasks.config import config


class ConfigParameter(luigi.OptionalParameter):
    pass


class TaskBase(luigi.Task):
    config_parameters = ()
    now = luigi.DateMinuteParameter(default=datetime.now())

    @classmethod
    def get_param_values(cls, params, args, kwargs):
        task_family = cls.get_task_family()
        for param_name, param_obj in params:
            if not isinstance(param_obj, ConfigParameter):
                continue
            if param_obj.has_task_value(task_family, param_name):
                continue
            if param_name in kwargs:
                continue
            param_value = getattr(config, param_name, None)
            if not param_value:
                continue
            kwargs[param_name] = getattr(config, param_name)
        param_values = super().get_param_values(params, args, kwargs)
        return param_values

    @cached_property
    def output_path(self):
        if not os.path.exists(config.cachedir):
            os.makedirs(config.cachedir)

        return os.path.join(config.cachedir, "{}.out".format(self.task_id))

    def get_task(self, name, **kw):
        return luigi.task_register.Register.get_task_cls(name)(**kw)

    def output(self):
        return luigi.LocalTarget(self.output_path)
