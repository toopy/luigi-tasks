import json

import luigi
import luigi_tasks
from cached_property import cached_property
from luigi_tasks.tasks.base import TaskBase


class CheckBasedWorkflow(TaskBase):
    update_tasks = luigi.DictParameter()

    @cached_property
    def check_result(self):
        with self.input().open("r") as f_in:
            return json.loads(f_in.read().strip())

    @property
    def need_update(self):
        raise NotImplementedError()

    def requires(self):
        raise NotImplementedError()

    def run(self):
        status = "nothing to do"

        if self.need_update:
            for task_cls, params in self.update_tasks.items():
                yield getattr(luigi_tasks, task_cls)(
                    **{p: getattr(self, p) for p in params}
                )
            status = "updated"

        with self.output().open("w") as f:
            f.write("{}\n".format(status))
