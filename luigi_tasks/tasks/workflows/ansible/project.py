import luigi
from luigi_tasks.tasks.base import ConfigParameter
from luigi_tasks.tasks.workflows.version import CheckVersionBasedWorkflow


class UpdateProject(CheckVersionBasedWorkflow):
    extra_vars = luigi.DictParameter(default={})
    hosts = luigi.ListParameter(default=())
    inventory = luigi.OptionalParameter(default="")
    options = luigi.OptionalParameter(default="")
    project_dir = luigi.OptionalParameter(default="")
    update_tasks = luigi.DictParameter(
        default={
            "UpdateGithubFolder": ("version",),
            "UpdateGalaxyRoles": (),
            "AnsiblePlaybook": (
                "extra_vars",
                "hosts",
                "inventory",
                "options",
                "playbook",
                "project_dir",
            ),
        }
    )

    playbook = ConfigParameter()
