import luigi
from luigi_tasks.tasks.ansible.command import RunCommand
from luigi_tasks.tasks.base import ConfigParameter
from luigi_tasks.tasks.workflows.base import CheckBasedWorkflow


class CheckCommandBasedWorkflow(CheckBasedWorkflow):
    hosts = luigi.ListParameter(default=())
    inventory = luigi.OptionalParameter(default="")

    command = ConfigParameter()

    @property
    def need_update(self):
        # need update if command fail
        return self.check_result["code"]

    def requires(self):
        return RunCommand(
            command=self.command,
            hosts=self.hosts,
            ignore_errors=True,
            inventory=self.inventory,
        )
