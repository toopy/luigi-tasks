import luigi
from luigi_tasks.tasks.base import ConfigParameter
from luigi_tasks.tasks.workflows.ansible.command import CheckCommandBasedWorkflow
from luigi_tasks.tasks.workflows.version import CheckVersionBasedWorkflow


class GithubIssueOnMixin:
    update_tasks = luigi.DictParameter(
        default={"CreateGithubIssue": ("issue_body", "owner", "repo", "token",),}
    )

    issue_body = luigi.Parameter()
    owner = ConfigParameter()
    repo = ConfigParameter()
    token = ConfigParameter()


class GithubIssueOnCheck(GithubIssueOnMixin, CheckVersionBasedWorkflow):
    pass


class GithubIssueOnCommand(GithubIssueOnMixin, CheckCommandBasedWorkflow):
    pass


class JiraIssueOnMixin:
    update_tasks = luigi.DictParameter(
        default={
            "CreateJiraIssue": (
                "description",
                "issue_type",
                "password",
                "project",
                "summary",
                "url",
                "username",
            ),
        }
    )
    description = luigi.Parameter()
    issue_type = luigi.Parameter(default="Task")
    password = ConfigParameter()
    project = ConfigParameter()
    summary = luigi.Parameter()
    url = ConfigParameter()
    username = ConfigParameter()


class JiraIssueOnCheck(JiraIssueOnMixin, CheckVersionBasedWorkflow):
    pass


class JiraIssueOnCommand(JiraIssueOnMixin, CheckCommandBasedWorkflow):
    pass
