import json
import logging
import re

import luigi
from cached_property import cached_property
from luigi_tasks.tasks.base import TaskBase
from luigi_tasks.tasks.workflows.base import CheckBasedWorkflow

logger = logging.getLogger(__name__)


class CheckVersion(TaskBase):
    current_task = luigi.Parameter()
    current_regex = luigi.Parameter(default="(.*)")
    current_result_field = luigi.Parameter("version")

    latest_task = luigi.Parameter()
    latest_regex = luigi.Parameter(default="(.*)")
    latest_result_field = luigi.Parameter("version")

    hosts = luigi.ListParameter(default=())
    inventory = luigi.OptionalParameter(default="")

    def _get_version(self, key, result_field):
        with self.input()[key].open("r") as f_in:
            return json.loads(f_in.read().strip())[result_field]

    @cached_property
    def current_version(self):
        return re.search(
            self.current_regex,
            self._get_version("current_task", self.current_result_field),
        )[1]

    @cached_property
    def latest_version(self):
        return re.search(
            self.latest_regex,
            self._get_version("latest_task", self.latest_result_field),
        )[1]

    def requires(self):
        kw = dict(hosts=self.hosts, inventory=self.inventory,)
        return {
            "current_task": self.get_task(self.current_task, **kw,),
            "latest_task": self.get_task(self.latest_task, **kw,),
        }

    def run(self):
        with self.output().open("w") as f:
            versions = {
                "current_version": self.current_version,
                "latest_version": self.latest_version,
            }
            logger.debug("versions: %s", versions)
            f.write("{}\n".format(json.dumps(versions)))


class CheckVersionBasedWorkflow(CheckBasedWorkflow):
    hosts = luigi.ListParameter(default=())
    inventory = luigi.OptionalParameter(default="")
    target_version = luigi.OptionalParameter(default="")

    @property
    def need_update(self):
        return (
            self.check_result["current_version"] != self.check_result["latest_version"]
        )

    @property
    def version(self):
        return self.target_version or self.check_result["latest_version"]

    def requires(self):
        return CheckVersion(hosts=self.hosts, inventory=self.inventory,)
