import json
import logging
import os
import subprocess
import sys
from tempfile import NamedTemporaryFile

import luigi
from luigi_tasks.config import config
from luigi_tasks.tasks.base import TaskBase
from luigi_tasks.tasks.constants import INVENTORIES_DIR, PLAYBOOKS_DIR, ROOT

logger = logging.getLogger(__name__)


class AnsiblePlaybook(TaskBase):
    extra_vars = luigi.DictParameter(default={})
    hosts = luigi.ListParameter(default=())
    inventory = luigi.OptionalParameter(default="")
    options = luigi.OptionalParameter(default="")
    playbook = luigi.Parameter()
    project_dir = luigi.OptionalParameter(default="")
    verbosity = luigi.IntParameter(default=0)

    is_local = False
    playbooks_dir = PLAYBOOKS_DIR

    @property
    def default_hosts(self):
        return ("luigi",)

    @property
    def default_inventory(self):
        return os.path.join(INVENTORIES_DIR, "local", "inventory",)

    @property
    def ansible_extra_vars(self):
        return self.extra_vars or config.extra_vars

    @property
    def ansible_hosts(self):
        if self.is_local:
            return self.default_hosts
        else:
            return self.hosts or config.hosts or self.default_hosts

    @property
    def ansible_inventory(self):
        if self.is_local:
            return self.default_inventory
        else:
            return self.inventory or config.inventory or self.default_inventory

    @property
    def ansible_options(self):
        return self.options or config.options or "-vD"

    @property
    def ansible_project_dir(self):
        return self.project_dir or config.project_dir or ROOT

    @property
    def ansible_vars(self):
        return dict(
            ansible_options=self.ansible_options,
            ansible_pyenv=self.pyenv,
            luigi_hosts=self.ansible_hosts,
            luigi_output_path=self.output_path,
        )

    @property
    def pyenv(self):
        return os.path.dirname(os.path.dirname(sys.executable))

    def run(self):
        dumped_vars = json.dumps(dict(self.ansible_vars, **self.ansible_extra_vars,))

        logger.debug("extra_vars: %s", dumped_vars)

        with NamedTemporaryFile() as vars_file:

            vars_file.write(dumped_vars.encode("utf-8"))
            vars_file.seek(0)

            if not os.path.exists(self.ansible_project_dir):
                os.makedirs(self.ansible_project_dir)
            os.chdir(self.ansible_project_dir)

            command_args = [
                os.path.join(self.pyenv, "bin", "ansible-playbook"),
                "--extra-vars",
                "@{}".format(vars_file.name),
                "--inventory",
                self.ansible_inventory,
                self.playbook,
                self.ansible_options,
            ]

            logger.debug("command: %s", " ".join(command_args))

            proc = subprocess.Popen(
                command_args, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
            )

            out, err = proc.communicate()

        if proc.returncode != 0:
            raise RuntimeError(
                "\n stdout:\n---\n{}\n\n stderr:\n---\n{}".format(
                    out.decode("utf-8"), err.decode("utf-8"),
                )
            )

        logger.debug(out.decode("utf-8"))

        # already completed
        if os.path.exists(self.output_path):
            return

        with self.output().open("w") as f:
            f.write("done: {}\n".format(" ".join(command_args)))
