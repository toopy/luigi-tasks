import os

import luigi
from luigi_tasks.tasks.ansible.base import AnsiblePlaybook


class GetHttpVersion(AnsiblePlaybook):
    playbook = luigi.Parameter(
        default=os.path.join(AnsiblePlaybook.playbooks_dir, "get_http_version.yml")
    )
    domain = luigi.Parameter()
    json_query = luigi.Parameter(default="version")

    is_local = True

    @property
    def ansible_vars(self):
        return dict(
            super().ansible_vars,
            http_domain=self.domain,
            http_json_query=self.json_query,
        )
