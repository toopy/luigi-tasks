import os

import luigi
from luigi_tasks.tasks.ansible.base import AnsiblePlaybook
from luigi_tasks.tasks.base import ConfigParameter


class GetGithubLocalVersion(AnsiblePlaybook):
    playbook = luigi.Parameter(
        default=os.path.join(
            AnsiblePlaybook.playbooks_dir, "get_github_local_version.yml"
        )
    )

    branch = ConfigParameter()
    project_dir = ConfigParameter()

    is_local = True

    @property
    def ansible_vars(self):
        return dict(
            super().ansible_vars,
            github_branch=self.branch,
            github_project_dir=self.project_dir,
        )


class GithubMixin:
    owner = ConfigParameter()
    repo = ConfigParameter()

    is_local = True

    @property
    def ansible_vars(self):
        return dict(
            super().ansible_vars, github_owner=self.owner, github_repo=self.repo,
        )


class GithubAPIMixin(GithubMixin):
    token = ConfigParameter()

    @property
    def ansible_vars(self):
        return dict(super().ansible_vars, github_token=self.token,)


class CreateGithubIssue(GithubAPIMixin, AnsiblePlaybook):
    playbook = luigi.Parameter(
        default=os.path.join(AnsiblePlaybook.playbooks_dir, "create_github_issue.yml")
    )
    issue_body = luigi.Parameter()

    @property
    def ansible_vars(self):
        return dict(super().ansible_vars, github_issue_body=self.issue_body,)


class GetGithubTag(GithubAPIMixin, AnsiblePlaybook):
    playbook = luigi.Parameter(
        default=os.path.join(AnsiblePlaybook.playbooks_dir, "get_github_tag.yml")
    )


class GetGithubVersion(GithubAPIMixin, AnsiblePlaybook):
    playbook = luigi.Parameter(
        default=os.path.join(AnsiblePlaybook.playbooks_dir, "get_github_version.yml")
    )
    branch = ConfigParameter()

    @property
    def ansible_vars(self):
        return dict(super().ansible_vars, github_branch=self.branch,)


class UpdateGithubFolder(GithubMixin, AnsiblePlaybook):
    playbook = luigi.Parameter(
        default=os.path.join(AnsiblePlaybook.playbooks_dir, "update_github_folder.yml")
    )
    project_dir = ConfigParameter(default="")
    version = ConfigParameter(default="")

    @property
    def ansible_vars(self):
        return dict(
            super().ansible_vars,
            github_project_dir=self.project_dir,
            github_version=self.version,
        )
