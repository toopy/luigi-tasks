import os

import luigi

from .base import AnsiblePlaybook


class RunCommand(AnsiblePlaybook):
    playbook = luigi.Parameter(
        default=os.path.join(AnsiblePlaybook.playbooks_dir, "run_command.yml")
    )
    command = luigi.Parameter()
    ignore_errors = luigi.BoolParameter(default=False)

    @property
    def ansible_vars(self):
        return dict(
            super().ansible_vars,
            command=self.command,
            command_ignore_errors=self.ignore_errors,
        )
