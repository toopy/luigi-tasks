import os

import luigi
from luigi_tasks.tasks.ansible.base import AnsiblePlaybook
from luigi_tasks.tasks.base import ConfigParameter


class GetPkgVersion(AnsiblePlaybook):
    playbook = luigi.Parameter(
        default=os.path.join(AnsiblePlaybook.playbooks_dir, "get_pkg_version.yml")
    )
    package = ConfigParameter()

    is_local = False

    @property
    def ansible_vars(self):
        return dict(super().ansible_vars, package=self.package,)


class GetPkgAvailableVersion(GetPkgVersion):
    search = luigi.Parameter(default="candidat")
    update_cache = luigi.BoolParameter(
        default=True, parsing=luigi.BoolParameter.EXPLICIT_PARSING,
    )

    @property
    def ansible_vars(self):
        return dict(
            super().ansible_vars, update_cache=self.update_cache, search=self.search,
        )


class GetPkgInstalledVersion(GetPkgVersion):
    search = luigi.Parameter(default="install")

    @property
    def ansible_vars(self):
        return dict(super().ansible_vars, search=self.search,)
