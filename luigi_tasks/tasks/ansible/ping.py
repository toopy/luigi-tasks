import os

import luigi
from luigi_tasks.tasks.ansible.base import AnsiblePlaybook
from luigi_tasks.tasks.base import ConfigParameter


class AnsiblePing(AnsiblePlaybook):
    playbook = luigi.Parameter(
        default=os.path.join(AnsiblePlaybook.playbooks_dir, "ping.yml")
    )
    ping = ConfigParameter()

    is_local = True

    @property
    def ansible_vars(self):
        return dict(super().ansible_vars, ping=self.ping,)
