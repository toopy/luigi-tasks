from luigi_tasks.tasks.ansible.base import AnsiblePlaybook
from luigi_tasks.tasks.ansible.circle import GetCircleVersion
from luigi_tasks.tasks.ansible.command import RunCommand
from luigi_tasks.tasks.ansible.galaxy import UpdateGalaxyRoles
from luigi_tasks.tasks.ansible.github import (
    CreateGithubIssue,
    GetGithubLocalVersion,
    GetGithubTag,
    GetGithubVersion,
    UpdateGithubFolder,
)
from luigi_tasks.tasks.ansible.http import GetHttpVersion
from luigi_tasks.tasks.ansible.jira import CreateJiraIssue
from luigi_tasks.tasks.ansible.ping import AnsiblePing
from luigi_tasks.tasks.ansible.pkg import GetPkgAvailableVersion, GetPkgInstalledVersion

__all__ = (
    "AnsiblePing",
    "AnsiblePlaybook",
    "CreateGithubIssue",
    "CreateJiraIssue",
    "GetCircleVersion",
    "GetGithubLocalVersion",
    "GetGithubTag",
    "GetGithubVersion",
    "GetHttpVersion",
    "GetPkgInstalledVersion",
    "GetPkgAvailableVersion",
    "RunCommand",
    "UpdateGalaxyRoles",
    "UpdateGithubFolder",
)
