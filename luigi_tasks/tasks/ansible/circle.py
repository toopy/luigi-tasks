import os

import luigi
from luigi_tasks.tasks.ansible.base import AnsiblePlaybook
from luigi_tasks.tasks.base import ConfigParameter


class GetCircleVersion(AnsiblePlaybook):
    job = luigi.Parameter()
    playbook = luigi.Parameter(
        default=os.path.join(AnsiblePlaybook.playbooks_dir, "get_circle_version.yml")
    )

    branch = ConfigParameter()
    owner = ConfigParameter()
    repo = ConfigParameter()
    token = ConfigParameter()

    is_local = True

    @property
    def ansible_vars(self):
        return dict(
            super().ansible_vars,
            circle_branch=self.branch,
            circle_job=self.job,
            circle_owner=self.owner,
            circle_repo=self.repo,
            circle_token=self.token,
        )
