import os

import luigi
from luigi_tasks.tasks.ansible.base import AnsiblePlaybook
from luigi_tasks.tasks.base import ConfigParameter


class UpdateGalaxyRoles(AnsiblePlaybook):
    playbook = luigi.Parameter(
        default=os.path.join(AnsiblePlaybook.playbooks_dir, "update_galaxy_roles.yml")
    )
    extra_options = luigi.Parameter(default="-f")
    requirements = luigi.Parameter(default="requirements.yml")
    roles_dir = luigi.Parameter(default="roles")

    project_dir = ConfigParameter()

    is_local = True

    @property
    def ansible_vars(self):
        return dict(
            super().ansible_vars,
            galaxy_extra_options=self.extra_options,
            galaxy_project_dir=self.project_dir,
            galaxy_requirements=self.requirements,
            galaxy_roles_dir=self.roles_dir,
        )
