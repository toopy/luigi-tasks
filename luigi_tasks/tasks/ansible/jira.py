import os

import luigi
from luigi_tasks.tasks.ansible.base import AnsiblePlaybook
from luigi_tasks.tasks.base import ConfigParameter


class JiraMixin:
    password = ConfigParameter()
    project = ConfigParameter()
    url = ConfigParameter()
    username = ConfigParameter()

    is_local = True

    @property
    def ansible_vars(self):
        return dict(
            super().ansible_vars,
            jira_password=self.password,
            jira_project=self.project,
            jira_url=self.url,
            jira_username=self.username,
        )


class CreateJiraIssue(JiraMixin, AnsiblePlaybook):
    playbook = luigi.Parameter(
        default=os.path.join(AnsiblePlaybook.playbooks_dir, "create_jira_issue.yml")
    )
    description = luigi.Parameter()
    issue_type = luigi.Parameter(default="Task")
    summary = luigi.Parameter()

    @property
    def ansible_vars(self):
        return dict(
            super().ansible_vars,
            jira_description=self.description,
            jira_issue_type=self.issue_type,
            jira_summary=self.jira_summary,
        )
