import os

ROOT = os.path.abspath(
    os.path.join(os.path.dirname(os.path.abspath(__file__)), "ansible",)
)

INVENTORIES_DIR = os.path.join(ROOT, "inventories",)

PLAYBOOKS_DIR = os.path.join(ROOT, "playbooks",)
