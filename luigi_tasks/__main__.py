import os

import luigi


def run():
    luigi_config_path = os.environ.pop("LUIGI_CONFIG_PATH", "")
    luigi_config_path_list = [p for p in luigi_config_path.split(",") if p]

    for path in luigi_config_path_list:
        luigi.configuration.add_config_path(path)

    luigi.run()
