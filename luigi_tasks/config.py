import os

import luigi


def _d(name, default=""):
    prefix = os.environ.get("LUIGI_TASKS_PREFIX", "LUIGI_TASKS")
    return os.environ.get("{}_{}".format(prefix, name), default)


class luigi_tasks(luigi.Config):
    cachedir = luigi.Parameter(default=_d("CACHEDIR", default="~/.cache/luigi_tasks"))
    branch = luigi.OptionalParameter(default=_d("BRANCH"))
    extra_vars = luigi.DictParameter(default={})
    hosts = luigi.ListParameter(default=())
    inventory = luigi.OptionalParameter(default=_d("INVENTORY"))
    issue_body = luigi.OptionalParameter(default=_d("ISSUE_BODY"))
    options = luigi.OptionalParameter(default=_d("OPTIONS"))
    owner = luigi.OptionalParameter(default=_d("OWNER"))
    package = luigi.OptionalParameter(default=_d("PACKAGE"))
    password = luigi.OptionalParameter(default=_d("PASSWORD"))
    ping = luigi.OptionalParameter(default=_d("PING"))
    playbook = luigi.OptionalParameter(default=_d("PLAYBOOK"))
    project = luigi.OptionalParameter(default=_d("PROJECT"))
    project_dir = luigi.OptionalParameter(default=_d("PROJECT_DIR"))
    repo = luigi.OptionalParameter(default=_d("REPO"))
    token = luigi.OptionalParameter(default=_d("TOKEN"))
    username = luigi.OptionalParameter(default=_d("USERNAME"))
    version = luigi.OptionalParameter(default=_d("VERSION"))


config = luigi_tasks()
